# GIT TP remote - travail collaboratif 


## Objectif du TP 

### Version 1 

1. Faites des groupes de 3~4 personnes 
2. Une personne du groupe s'occupe de créer un dépôt sur gitlab 
3. Le créateur du dépôt invite ses collaborateurs dans le projet 
4. Chaque collaborateur clone le dépôt localement 
5. Chacune renseigne son nom dans le Fichier README.md 
6. Chacun pousse (attention le premier qui pousse gagne)
7. Faites en sorte qu'à la fin le fichier contienne le nom de chacun 


### Version 2 

Reprendre le scénario de la version 1 mais essayez de faire les choses un peu plus finement en utilisant les branches et Merge Request 




